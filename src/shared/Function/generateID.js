import uuid from 'uuid/v1'

const generateID = ( ) =>{
    return uuid()
}

export default generateID;