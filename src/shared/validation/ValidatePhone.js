const validatePhone = (p) => {
    var phoneno = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    return phoneno.test(p) && phoneno.test(p) <= 11;
}

module.exports = validatePhone;