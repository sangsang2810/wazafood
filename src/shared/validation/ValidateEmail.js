const ValidateEmail = (mail) => {
    // eslint-disable-next-line no-useless-escape
    let reg = /^[a-z]\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return reg.test(mail) && mail.charAt(0)=== mail.charAt(0).toLowerCase();
}
module.exports = ValidateEmail;