const checkNull = (value) =>{
    if(value === '' || value === "undefined" || value === null){
        return true;
    }
    return false;
}

module.exports = checkNull;