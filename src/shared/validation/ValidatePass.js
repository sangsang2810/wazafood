const checkPass = (e)=>{
    var passw = /[0-9A-Za-z]/g;
    if(passw.test(e) && e.length <= 6){
        alert('Mật khẩu phải có độ dài từ 6 trở lên')
        return true;
    }
    return false;
}
module.exports = checkPass;