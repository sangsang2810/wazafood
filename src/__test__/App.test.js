var checkNull = require('../shared/validation/ValidateForm');
var validateCfmPass = require('../shared/validation/validateCfmPass');
var validateEmail = require('../shared/validation/ValidateEmail');
var checkPass = require('../shared/validation/ValidatePass'); 
var validatePhone = require('../shared/validation/validatePhone'); 


// checkNull
test('test check null : case 1 _ " " ', () => {
    let value = '';
    expect(checkNull(value)).toBe(true);
})
test('test check null : case 2 _ null ', () => {
    let value = null;
    expect(checkNull(value)).toBe(true);
})
test('test check null : case 3 _ "undefined" ', () => {
    let value = "undefined";
    expect(checkNull(value)).toBe(true);
})
test('test check null : case 4 _ "value" ', () => {
    let value = "value";
    expect(checkNull(value)).toBe(false);
})
test('test check null : case 5 _ "456" ', () => {
    let value = "456";
    expect(checkNull(value)).toBe(false);
})

// validateEmail
test('test validate email : case 1 _ sanghh@gmail.com', () => {
    let email = 'sanghh@gmail.com';
    expect(validateEmail(email)).toBe(true);
})
test('test validate email : case 2 _ sanghh@gmail', () => {
    let email = 'sanghh@gmail';
    expect(validateEmail(email)).toBe(false);
})
test('test validate email : case 3 _ sanghh', () => {
    let email = 'sanghh';
    expect(validateEmail(email)).toBe(false);
})
test('test validate email : case 4 _ %^&*@gmai.com', () => {
    let email = '%^&*@gmai.com';
    expect(validateEmail(email)).toBe(false);
})
test('test validate email : case 5 _ 12356858@gmail.com', () => {
    let email = '12356858@gmail.com';
    expect(validateEmail(email)).toBe(false);
})
test('test validate email : case 6 _ 1222sanghh@gmail.com', () => {
    let email = '1222sanghh@gmail.com';
    expect(validateEmail(email)).toBe(false);
})
test('test validate email : case 7 _ saghhh25215@gmail.com', () => {
    let email = 'saghhh25215@gmail.com';
    expect(validateEmail(email)).toBe(true);
})

// checkPass
test('test validate pass : case 1 _ 12345', () => {
    let pass = '12345';
    expect(checkPass(pass)).toBe(true);
})
test('test validate pass : case 2 _ 1234567', () => {
    let pass = '1234567';
    expect(checkPass(pass)).toBe(false);
})

// validatePhone
test('test validate phone : case 1 _ 0912546935', () => {
    let phone = '0912546935';
    expect(validatePhone(phone)).toBe(true);
})
test('test validate phone : case 2 _ 09125469351', () => {
    let phone = '09125469351';
    expect(validatePhone(phone)).toBe(true);
})
test('test validate phone : case 3 _ 0712546935', () => {
    let phone = '0712546935';
    expect(validatePhone(phone)).toBe(true);
})
test('test validate phone : case 4 _ 0812546935 ', () => {
    let phone = '0812546935 ';
    expect(validatePhone(phone)).toBe(true);
})
test('test validate phone : case 5 _ 0512546935', () => {
    let phone = '0512546935';
    expect(validatePhone(phone)).toBe(true);
})
test('test validate phone : case 6 _ 1254785236', () => {
    let phone = '1254785236';
    expect(validatePhone(phone)).toBe(false);
})
test('test validate phone : case 7 _ 21as547852', () => {
    let phone = '21as547852';
    expect(validatePhone(phone)).toBe(false);
})
test('test validate phone : case 8 _ 097521548632', () => {
    let phone = '097521548632';
    expect(checkPass(phone)).toBe(false);
})
test('test validate phone : case 9 _ 1235245s', () => {
    let phone = '1235245s';
    expect(validatePhone(phone)).toBe(false);
})
// validateCfmPass
test('test validate Confirm Password : case 1 _ a514653,a514653', () => {
    let x = 'aswad25';
    let y = 'aswad25';
    expect(validateCfmPass(x,y)).toBe(true);
})