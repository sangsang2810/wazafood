import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";

import HomePage from './Component/HomePage/HomePage';
import FoodPage from './Component/FoodPage/FoodPage';
import DrinkPage from './Component/DrinkPage/DrinkPage';
import ServicePage from './Component/ServicePage/ServicePage';
import Account from './Component/Account/Account';
import FoodDetail from './Component/FoodDetail/FoodDetail'
import SignUp from './Component/SignUp/SignUp';
import DetailAcc from './Component/DetailsAccount/DetailAcc'
import CartLst from './Component/CartPage/CartLst'
import PaymentMethod from './Component/PaymentV2/PaymentMethod';
function App() {
  return (
    <div className="App">
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/foodpage" component={FoodPage} />
        <Route path="/cart" component={CartLst} />
        <Route path="/drinkpage" component={DrinkPage} />
        <Route path="/servicepage" component={ServicePage} />
        <Route path="/account" component={Account} />
        <Route path="/fooddetail" component={FoodDetail} />
        <Route path="/signup" component={SignUp} />
        <Route path="/service" component={ServicePage} />
        <Route path="/detailsAccount" component={DetailAcc}/>
        <Route path="/paymentMethod" component={PaymentMethod}/>
    </Switch>
    </BrowserRouter>
    </div>
  );
}

export default App;
