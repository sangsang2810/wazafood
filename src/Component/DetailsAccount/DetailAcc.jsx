import React, { Component } from 'react';
import NavBottom from '../HomePage/NavBottom';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import Input from '@material-ui/core/Input';
import { Button } from '@material-ui/core';
import axios from 'axios';


export default class DetailAcc extends Component {
    constructor(props){
        super(props)
        this.state={
            token: localStorage.getItem('token'),
            user : JSON.parse(localStorage.getItem('user')),
            userInf: {},
        }
    }
    userInf = async () => {
        const res = await axios({
            method:'GET',
            url:`https://waza-passenger.herokuapp.com/api/passengers/${this.state.user._id}`,
            headers: {
                Authorization : this.state.token
            },
            // data :this.state.user._id,
          });
        this.setState({userInf : res.data});
        console.log(this.state.userInf);
    }
    componentDidMount(){
        this.userInf();
    }
    deCode = () => {
        var jwt = require('jsonwebtoken');
        var token = this.state.token;
        var deCode = jwt.decode(token);
        return localStorage.setItem('user',JSON.stringify(deCode) || {} )
    }

    render() {
        
        
        return (
            <div>
                <AccountBoxIcon style={{fontSize:'15vh'}} color="disabled"/>

                <NavBottom/>

                <div style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                <Input
                    style={{width:'70vw',marginBottom:'10px',color:'black'}}
                    value={`${this.state.userInf.fullName}`}
                    disabled
                    inputProps={{
                    'aria-label': 'description',
                    }}
                />
                <Input
                    style={{width:'70vw', marginBottom:'10px',color:'black'}}
                    value={`${this.state.userInf.email}`}
                    disabled
                    inputProps={{
                    'aria-label': 'description',
                    }}
                />
                <Input
                    style={{width:'70vw',marginBottom:'10px',color:'black'}}
                    value={`${this.state.userInf.phone}`}
                    disabled
                    inputProps={{
                    'aria-label': 'description',
                    }}
                />
                <Input
                    style={{width:'70vw',marginBottom:'10px',color:'black'}}
                    value={`${this.state.userInf.updatedAt}`}
                    disabled
                    inputProps={{
                    'aria-label': 'description',
                    }}
                />
                <Button color="secondary"
                    style={{width:'70vw',marginTop:'10px',outline:'none'}} >
                    <b>Chỉnh sửa thông tin</b>
                </Button>
                </div>
            </div>
        )
    }
}
