import React, { Component } from 'react'
import Menu from '../HomePage/MenuBar/Menu'
import './FoodDetail.css'
import NavBottom from '../HomePage/NavBottom'
import  qs from 'qs'
// Card
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
// Button
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';




export default class FoodDetail extends Component {
    constructor(props){
        super(props)
        this.state={
            id:localStorage.getItem('restID') ||localStorage.setItem('restID', 1),
            foodID: localStorage.getItem('foodID') ||localStorage.setItem('foodID', 1),
            restaurantDto:{},
            foodList:[],
            restaurantImage:"",
            restID: localStorage.getItem('restID'),
        }
    }

    fetchRestaurant = async () => {
        const res = await axios({
            method:'POST',
            url:'http://103.82.240.199:9080/final-excercise/api/restaurant',
            data:qs.stringify({restaurantId : '1'}),
            headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
          });
        console.log(res.data)
        this.setState({restaurantDto : res.data.restaurantDto})
        this.setState({foodList : res.data.foodList})
        this.setState({restaurantImage : res.data.restaurantDto.restaurantImage })
    }
    componentDidMount(){
        this.fetchRestaurant();
    }

    findIdexOfArr = (id,arr,num) => {
        var index = arr.map(e => {
            return num === 1 ? e.restaurantId :e.id
        }).indexOf(Number(id)) 
        return index;
    }

    addOnClick = () => {
        const foodID = localStorage.getItem('foodID');

        const findFood = this.state.foodList.find(findFood => findFood.id === Number(foodID));
        const findFoodAmount = Object.assign({},findFood)

        let id = findFoodAmount.id;
        let price =findFoodAmount.price;
        // let image =findFoodAmount.image;
        let name_food = findFoodAmount.name_food;
        if (typeof(Storage) !== 'undefined') {
            let cart = JSON.parse(sessionStorage.getItem('cart')) || sessionStorage.setItem('cart', JSON.stringify({})) || {};
            // cart[id] = (cart[id] ? cart[id]: 0);
            if (cart[id]){
                cart[id].amount++;
            } else {
            cart[id] = { id , price, amount: 1 , name_food};
            }
            sessionStorage.setItem('cart',JSON.stringify(cart));
        } else {
            //Nếu không hỗ trợ
            alert('Trình duyệt của bạn không hỗ trợ Storage');
        }
    }

    render() {
        return (
            <div style={{marginBottom:'50px'}} >
               <div>
                    <div className="food-detail">
                        <div>
                        <Menu/>
                        <div className="restaurant-detail">
                        <div>
                            <img alt="" src={this.state.restaurantImage} style={{width:'100vw',backgroundRepeat:'no-repeat',backgroundSize:'auto'}} />
                        </div>
                            <div className="infRestaurant">
                            <ul >
                            <li className="txt-RDetail"><h2><b>
                                {this.state.restaurantDto.restaurant_name}
                                </b></h2>
                            </li>
                            <li className="txt-RDetail">
                                Đ/c: {this.state.restaurantDto.restaurant_address}
                            </li>
                            <li className="txt-RDetail">
                                Giờ mở cửa: 
                            </li>
                            <li className="txt-RDetail">
                                Trạng thái:  
                            </li>
                            </ul>
                            </div>
                        </div>
                        <div style={{marginTop:'20px'}}>
                            <h5>Menu</h5>
                        </div>
                        </div>
                    </div>
                </div>

                <div>
                    {
                        this.state.foodList.map((e,i) => {
                            return  <Card key={i}  component="div" style={{display:'flex', marginTop:'10px'}}>
                                <CardMedia
                                    style={{width:'100px'}}
                                    component="img"
                                    title="Food Image"
                                    src="http://media.designs.vn/public/media/media/picture/04-07-2015/hinh-nen-do-an-cho-desktop-14.jpg">
                                </CardMedia>

                                <CardContent>
                                    <Typography component={'div'}>
                                        <div>
                                        <Box 
                                        fontWeight={500}
                                        textAlign="left"
                                        fontSize={16}>
                                            {e.name_food}
                                        </Box>
                                        </div>
                                    </Typography>
                                    <Typography component={'div'} color="textSecondary">
                                        <Box 
                                        textAlign="left"
                                        fontSize={12}>
                                            FOOD or DRINK Description
                                        </Box>
                                    </Typography>
                                </CardContent>

                                <Typography component="h5" variant="h5">
                                    {e.price}
                                </Typography>

                                <IconButton onClick={() => {
                                    localStorage.setItem('foodID',e.id);
                                    this.addOnClick()
                                    }} 
                                    style={{outline:'none',}} >
                                    <AddShoppingCartIcon color="primary"/> 
                                </IconButton>
                                
                            </Card>
                        })
                    }
                    </div>
               <NavBottom/>
            </div>
        )
    }
}
