import React, { Component } from 'react'
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import './MenuItems.css'
import dataItems from '../../../contants/food'

export default class MenuItem extends Component {
    constructor (props){
        super(props)
        this.state={
        }
    }       

    render() {
        const restaurantFilter = dataItems.filter(x => x.restaurant === this.props.filter);
        return  <div className="head">
            <div id="menuItems">
                {
                    restaurantFilter.map((x,i) => {
                        return(
                            <ListItems key={i} image={x.image} title={x.title}  price={x.price} id={x.id}/>
                        )
                    })
                }
            </div>
        </div>
    }
}

const ListItems = (props) => {
        // console.log(this.props.title, this.props.text)
    return(
        <article  className="Items">
            <Items image={props.image} title={props.title} price={props.price} id={props.id} />
        </article>
    )
}

const Items = (props) => {

    const handleAddToCart = (e) =>{
        // Constructer
        var image= props.image;
        var title= props.title;
        var price= props.price;
        var id = props.id;

        if (typeof(Storage) !== 'undefined') {
            //Thực hiện thao tác với Storage

            //Số lượng sản phẩm trong cart
            // if(localStorage.quantity){
            //     localStorage.quantity = Number(localStorage.quantity)+1;
            // }
            // else {
            //     localStorage.quantity = 1;
            // }
            
            //Add to cart 
            var refresh =localStorage.getItem("refresh");
            localStorage.setItem('refresh', refresh)

            var follow = JSON.parse(localStorage.getItem("follow")) || {}; // Khởi tạo mảng 1 chiều
            if(follow[id]){
                follow[id].amount++;
            }
            else{
                follow[id] = {id, price, image, amount: 1 , title}

            }
            localStorage.setItem("follow", JSON.stringify(follow));
           
        } else {
            //Nếu không hỗ trợ
            alert('Trình duyệt của bạn không hỗ trợ Storage');
        }
    }

    const {image} =props;
    var style={
        backgroundImage: 'url(' + image + ')',
        width: '110px',
        height: '110px'
    }
    
    return(
        <div className="footerItem">

            <header className="headerItem" style={style}></header>

            <h3 style={{textAlign:'left'}} className="bodyItem itemName">{props.title}</h3>

            <h3 className="itemPrice"><b>{props.price}</b></h3>
            <IconButton 
                onClick={handleAddToCart} 
                color="primary" 
                style={{ width:'100%', cursor:'pointer'}} 
                aria-label="add to shopping cart">
                <AddShoppingCartIcon />
            </IconButton>
        </div>
    )
}
