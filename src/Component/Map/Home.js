import React, { Component } from 'react';
import Map from './Map';

class Home extends Component {

	render() {
		return(
			<div style={{ margin: '100px' }}>
				<Map
					google={this.props.google}
					center={{lat: 10.775952, lng: 106.66748800000005
					}}
					height='300px'
					zoom={15}
				/>
			</div>
		);
	}
}

export default Home;
