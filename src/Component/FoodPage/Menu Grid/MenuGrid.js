// /* eslint-disable jsx-a11y/anchor-is-valid */
// import React, { Component } from 'react';
// import All from './All';
// import Breakfast from './Breakfast';
// import Lunch from './Lunch';
// import Dinner from './Dinner';
// // import Page from '../Pagess/Page';

// class MenuGrid extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { 
//           listFood: [
//             { id: 1, name: 'Food 1', details: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.', addr: 'District 1', price: 30 },
//             { id: 2, name: 'Food 2', details: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.', addr: 'District 2', price: 50 },
//             { id: 3, name: 'Food 3', details: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.', addr: 'District 3', price: 60 },            
//             { id: 4, name: 'Food 4', details: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.', addr: 'District 4', price: 90 },            
//           ],                    
//          }
//     }
//     render() { 
//       var {listFood} = this.state;
//         return ( 
//           <div>
//             <section className="food__menu__grid__area section-padding--lg">
//               <div className="container">
//                 <div className="row">
//                   <div className="col-lg-12">
//                     <div className="food__nav nav nav-tabs" role="tablist">
//                       <a className="active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab">All</a>
//                       <a id="nav-breakfast-tab" data-toggle="tab" href="#nav-breakfast" role="tab">Breakfast</a>
//                       <a id="nav-lunch-tab" data-toggle="tab" href="#nav-lunch" role="tab">Lunch</a>
//                       <a id="nav-dinner-tab" data-toggle="tab" href="#nav-dinner" role="tab">Dinner</a>                                      
//                     </div>
//                   </div>
//                 </div>

//                 <div className="row mt--30">
//                   <div className="col-lg-12">
//                     <div className="fd__tab__content tab-content" id="nav-tabContent">                      
//                       <All listFood={listFood} />                      
//                       {/* <Breakfast listFood={listFood} />
//                       <Lunch listFood={listFood} />                      
//                       <Dinner listFood={listFood} />                                              */}
//                     </div>
//                   </div>
//                 </div>
//                 {/* Page */}
//                 {/* <Page /> */}
//               </div>
//             </section>
//           {/* End Menu Grid Area */}
//         </div>
//          );
//     }
// }
 
// export default MenuGrid;