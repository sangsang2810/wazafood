import React, { Component } from 'react';
import FoodItem from './Fooditems';

class Breakfast extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        var {listFood} = this.props;
        var eleFood = listFood.map((listfood, index) =>{
            return <FoodItem key={listFood.id} index={index} listFood={listFood} />
        });
        return (     
            <div>
                 {/* Start Single Content */}
                 <div className="food__list__tab__content tab-pane fade show active" id="nav-all" role="tabpanel">                  
                  {/* Start Single Food */}
                    {eleFood}                    
                 </div>
                 {/* End Single Content */}
            </div>
        );
    }
}
 
export default Breakfast;