import React, { Component } from 'react'
import MenuGrid from './Menu Grid/MenuGrid';
import MenuBar from '../HomePage/MenuBar/Menu';

export default class FoodPage extends Component {
    render() {
        return (
            <div>
                <MenuBar />
                <MenuGrid />
            </div>
        )
    }
}
