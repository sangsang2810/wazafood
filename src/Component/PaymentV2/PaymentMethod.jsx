import React, { Component } from "react";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import { Grid, Typography, Box, Button } from "@material-ui/core";
import MapContainer from '../MapContainer/MapContainer'
import Geocode from "react-geocode";
import TextField from '@material-ui/core/TextField';


class PaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address:'',
      lat:0,
      lng:0,
      cart: JSON.parse(sessionStorage.getItem("cart"))
    };
  }

  geoCode = () =>{
    Geocode.setApiKey("AIzaSyCnBxsCyBZImkEf-mjyNIPA0fo4aER2IOw");
    Geocode.setLanguage("vi");
    Geocode.setRegion("vn");
    Geocode.enableDebug();
    Geocode.fromLatLng(this.state.lat, this.state.lng).then(
    response => {
      const address = response.results[0].formatted_address;
      console.log(address);
      this.setState({address: address});
    },
    error => {
      console.error(error);
    }
  );
}

  componentDidMount(){
    navigator.geolocation.getCurrentPosition(infor =>{
      this.setState({lat: infor.coords.latitude, lng: infor.coords.longitude})
    })
  }
  
  render() {
    let sum =0;
    if(this.state.cart){
        Object.values(this.state.cart).forEach(e => {
            sum += Number(e.price)*Number(e.amount);
        }) 
    }
    return (
      <div>
        
        <div>
        <div className="header" style={{ height: "8vh", width: "100vw" }}>
          <IconButton
            component={Link}
            to="/cart"
            style={{ outline: "none", marginLeft: "10px" }}
          >
            <ArrowBackIosIcon />
          </IconButton>
        </div>

        <Typography align="left">
          <b>Giao đến</b>
        </Typography>

        <div style={{minHeight:'300px', width:'100%'}}>
          <MapContainer  lat={this.state.lat} long={this.state.lng}/>
          <TextField id="standard-basic" label="Standard" />
        </div>



        <Typography align="left">
          <b>Tóm tắt đơn hàng</b>
        </Typography>
        <hr/>

        <div>
          <div style={{ display: "flex" }}>
            <Grid item xs={4}>
              <Typography component="div" align="center">
                <Box style={{ marginTop: "8px" }} fontWeight="fontWeightBold">
                  Tên món ăn
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography component="div" align="center">
                <Box style={{ marginTop: "8px" }} fontWeight="fontWeightBold">
                  Số lượng
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography component="div" align="center">
                <Box style={{ marginTop: "8px" }} fontWeight="fontWeightBold">
                  Giá tiền
                </Box>
              </Typography>
            </Grid>
          </div>
          <div>
            {Object.values(this.state.cart).map((e, i) => {
              return (
                <div key={i} style={{ display: "flex" }}>
                  <Grid item xs={4}>
                    <Typography component="div" align="center">
                      <Box style={{ marginTop: "8px" }}>{e.name_food}</Box>
                    </Typography>
                  </Grid>

                  <Grid item xs={3}>
                    <Typography component="div" align="center">
                      <Box style={{ marginTop: "8px", marginRight: "20px" }}>
                        {e.amount}
                      </Box>
                    </Typography>
                  </Grid>

                  <Grid item xs={3}>
                    <Typography component="div" align="center">
                      <Box style={{ marginTop: "8px" }}>
                        {`${Number(e.amount)}` * `${Number(e.price)}`}
                      </Box>
                    </Typography>
                  </Grid>
                </div>
              );
            })}
          </div>
        </div>
        <hr/>


        <Typography align="right">
          <b>Tổng tạm tính:</b> {typeof sum !== 'number' ? 0 : sum }
        </Typography>
        <Typography align="right">
          <b>Cước phí giao hàng: </b>tiền ship
        </Typography>
        <hr/>

        <Typography align="left">
          <b>Chọn phương thức thanh toán</b>
        </Typography>

        <Typography align="left">
          <b>Chọn khuyến mãi</b>
        </Typography>

        </div>

        <div
          style={{
            position: "fixed-inline",
            bottom: "0",
            top: "auto",
            width: "100vw"
          }}
        >
          <Button
            variant="contained"
            color="primary"
            size="large"
            style={{ width: "80vw",outline:'none' }}
          >
            <Typography>
              <b>Đặt đơn</b>
            </Typography>
          </Button>
        </div>

      </div>
    );
  }
}

export default PaymentMethod;
