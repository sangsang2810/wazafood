import React, { Component } from './node_modules/react'
import {Link} from './node_modules/react-router-dom'
import IconButton from './node_modules/@material-ui/core/IconButton';
import AddIcon from './node_modules/@material-ui/icons/Add';
import RemoveIcon from './node_modules/@material-ui/icons/Remove';
import DeleteIcon from './node_modules/@material-ui/icons/Delete';
import NavBottom from '../HomePage/NavBottom'
import './CartPage.css';

export default class CartPage extends Component {
    constructor (props){
        super(props)
        this.state={
            listCart:{},
            total:0, 
        }
    }     

    componentDidMount(){
        this.setState({listCart:JSON.parse(localStorage.getItem('follow'))})
        let sum = 0;
        this.setState({total:sum})  
    }

    removeHandleClick = () => {
        localStorage.removeItem("follow"); 
    }
    
    render() {    
        if(localStorage.getItem("refresh")){
            window.location.reload()
            localStorage.removeItem("refresh")
        }

        let sum =0;
        if(this.state.listCart){
            Object.values(this.state.listCart).forEach(e => {
                sum += Number(e.price)*Number(e.amount);
            }) 
        }

        return (
            <div className="listCart">
            <h1>{"So luong san pham " + localStorage.quantity}</h1>
            <button className="btnDes" 
                style={{background:'blue', width:'200px', height:'200px'}} 
                onClick={this.removeHandleClick}>
            </button>
  
            <div style={{display:'flex'}} className="tag">
                <h2 style={{width:'25vw'}}> </h2>
                <h2 style={{width:'25vw'}}>Tên món</h2>
                <h2 style={{width:'25vw'}}>Gía</h2>
            </div>

            <div >
            { this.state.listCart ?
                Object.values(this.state.listCart).map((x,i)=>{
                return(
                        <ListCart key={i} image={x.image} id={x.id}  title={x.title}  price={parseInt(x.price).toLocaleString()} amount={x.amount} />
                        )
                    }): <div></div>
            }
            </div>

                <div style={{display:'flex'}}>
                    <h2 style={{width:'200px'}}>Tổng tiền</h2>
                    <p>{typeof sum !== 'number' ? 0 : sum }</p>
                </div>

                <div>
                <Link to="/payment">
                    <button>Thanh Toan</button>
                </Link>
                </div>
                
                <NavBottom history={this.props.history}/>
            </div>
        )
    }
}

const ListCart =(props) =>{
    var id = props.id;
    const [amount ,setAmount] =React.useState(props.amount);
    const follow = JSON.parse(localStorage.getItem("follow")) ;
    
    const delHandleClick= () =>{
        let arr = Object.values(JSON.parse(localStorage.getItem('follow')));
        let del =arr.filter((cartArr) => {
            return cartArr.id !== props.id;
        })
        localStorage.setItem("follow", JSON.stringify({...del}));
        window.location.reload();
    }
    const amountPlus = () =>{
        console.log(follow[id])
        if(follow[id]){
            follow[id].amount++;
            console.log(follow[id].amount)
            window.location.reload();
        }
        setAmount(follow[id].amount)
        localStorage.setItem("follow", JSON.stringify(follow));
    }
    const amountMinus = () =>{        
        console.log(follow[id].amount)
        if(follow[id].amount === 1 ){
          return  delHandleClick()
        }
        else if(follow[id]){
            follow[id].amount--;
        }
        setAmount(follow[id].amount)
        localStorage.setItem("follow", JSON.stringify(follow));  
    }

    const {image} =props;
    var style={
        backgroundImage: 'url(' + image + ')',
        width: '100px',
        height: '100px'
    }

    return(
        <div className="formCart">
            <div style={{display:'flex'}}>  

            <div style={{width:'25vw',display:'flex',justifyContent:'center' }}>
                <header className="img" style={style}></header>
            </div> 

            <h3 style={{width:'25vw'}}>{props.title}</h3>
            <h4 style={{width:'25vw'}}>{props.price}</h4>
    
            <IconButton 
                style={{width:'50px', height:'50px'}} 
                onClick={amountMinus}>
            <RemoveIcon/>
            </IconButton>
            <h2 style={{width:'5vw'}}>{amount}</h2>  
            <IconButton 
                style={{width:'50px', height:'50px'}} 
                onClick={amountPlus}>
            <AddIcon/>
            </IconButton>

            <IconButton 
                style={{width:'50px', height:'50px'}}
                onClick={delHandleClick}>
            <DeleteIcon/>
            </IconButton>
            </div>
        </div>
    )
}


// console.log(this.state.items);
// const cartList = this.state.items.filter(x => x.category === this.props.filter);
// return (
//     <div>
//         {/* <Grid item xs={12}>
//             <Paper>Giỏ hàng</Paper>
//         </Grid> */}
//         <div>
//             {
//                 // cartList.map(item => {
//                 //     return item.lstFood.map((food,foodid) =>{
//                 //         return <div>
//                 //         <Grid item xs={3}>
//                 //             <Paper>img</Paper>
//                 //         </Grid>
                        
//                 //         <Grid item xs={3}>
//                 //             <Paper>{food.name_food}</Paper>
//                 //         </Grid>
                        
//                 //         <Grid item xs={3}>
//                 //         {food.price}
//                 //         </Grid>

//                 //         <Grid item xs={3}>
//                 //         {food.price}
//                 //         </Grid>
//                 //         </div> 
//                     })
//                 })
//             }
//         </div>
//     </div>
// )
// }
// }