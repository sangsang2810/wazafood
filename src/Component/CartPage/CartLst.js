import React, { Component } from 'react'
import NavBottom from "../HomePage/NavBottom";

import { Grid, Typography, Box,Button, IconButton, Paper } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import {Link} from "react-router-dom";

export default class CartLst extends Component {
    constructor (props) {
        super(props)
        this.state={
            cart: JSON.parse(sessionStorage.getItem('cart')),
            quantity: 1,
        }
    } 
    amountPlus = () =>{
        const foodID = localStorage.getItem('foodID');
        var id = Number(foodID)
        const cart = this.state.cart;
        if(cart[id]){
            cart[id].amount++;
            console.log(cart[id].amount)
            window.location.reload();
        }
        else if(cart[0]){
            cart[0].amount++;
            window.location.reload();
        }
        sessionStorage.setItem("cart", JSON.stringify(cart));
    }

    delHandleClick= () =>{
        const foodID = localStorage.getItem('foodID');
        var id = Number(foodID)
        let cart = Object.values(this.state.cart);
        let del = cart.filter((cartArr) => {
            return cartArr.id !== id;
        })
        sessionStorage.setItem("cart", JSON.stringify({...del}));
        window.location.reload();
    }

    amountMinus = () =>{
        const foodID = localStorage.getItem('foodID');
        var id = Number(foodID)
        const cart = this.state.cart;
        if(cart[id]){
            if(cart[id].amount === 1){
                return this.delHandleClick()
            }
            else if(cart[id]){
                cart[id].amount--;
                window.location.reload();   
            }
            sessionStorage.setItem("cart", JSON.stringify(cart));  
        }
        else if(cart[0].amount === 0){
            window.location.reload();
            return sessionStorage.removeItem('cart');
            
        }
        else if(cart[0]){
            cart[0].amount--;
            window.location.reload(); 
        }
        
        sessionStorage.setItem("cart", JSON.stringify(cart));
    }
    
    render() {
        // Cong Tien
        let sum =0;
        if(this.state.cart){
            Object.values(this.state.cart).forEach(e => {
                sum += Number(e.price)*Number(e.amount);
            }) 
        }
        return (
            <div>
                <div>
                {
                    this.state.cart ? (<div>
                        <Typography component="h4" variant="h4">Giỏ hàng</Typography>
                        <div style={{display:'flex', marginBottom:'20px',marginTop:'50px'}}>
                        <Grid item xs={6}>
                            <Typography component="div">
                            <Box fontWeight="fontWeightBold">
                                Tên sản phẩm
                            </Box>
                            </Typography>
                        </Grid>

                        <Grid item xs={3}>
                            <Typography  component="div">
                            <Box fontWeight="fontWeightBold">
                                Gía
                            </Box>
                            </Typography>
                        </Grid>

                        <Grid item xs={4}>
                            <Typography component="div">
                            <Box fontWeight="fontWeightBold">
                                Số lượng    
                            </Box>
                            </Typography>
                        </Grid>
                        </div>

                        <div>
                            {
                            Object.values(this.state.cart).map((e,i) => {
                                return <div key={i} style={{display:'flex'}}>
                                <Grid item xs={5}>
                                    <Typography component="div">
                                    <Box style={{ marginTop:'8px'}} fontWeight="fontWeightBold">
                                    {e.name_food}
                                    </Box>
                                    </Typography>
                                </Grid>
            
                                <Grid item xs={3}>
                                    <Typography  component="div">
                                    <Box style={{ marginTop:'8px'}} fontWeight="fontWeightBold">
                                    {e.price}
                                    </Box>
                                    </Typography>
                                </Grid>
            
                                <Grid item xs={4} style={{display:'flex'}} component="div">
                                    <Grid item xs={3}>
                                    <IconButton onClick={() => {
                                        localStorage.setItem('foodID',e.id);
                                        this.amountPlus(e.id)
                                        }} 
                                     style={{outline:'none'}} color="default">
                                        <AddIcon/>
                                    </IconButton>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Paper style={{marginTop:'8px',marginLeft:'15px'}}>{e.amount}</Paper>
                                    </Grid>
                                    <Grid item xs={3}>
                                    <IconButton onClick={() => {
                                        localStorage.setItem('foodID',e.id);
                                        this.amountMinus(e.id)
                                        }}
                                         style={{outline:'none'}} color="default">
                                        <RemoveIcon/>
                                    </IconButton>
                                    </Grid>
                                </Grid>
                            </div>
                            })
                            }
                        </div>
                        
                        <div style={{display:'flex', float:'right', marginTop:'30px'}}>
                        <h4>Tổng tiền: </h4>
                        <h4 style={{marginLeft:'10px',marginRight:'10px',marginBottom:'20px'}}> 
                            {typeof sum !== 'number' ? 0 : sum }</h4>
                        </div>
                        <Button
                        style={{background:'tomato',width:'100vw',color:'white',alignItems:'center'}}
                        component={Link} to="/paymentMethod"
                        >
                            <Typography >
                                Tiếp theo
                            </Typography>
                        </Button>

                    </div> ) : (
                        <img 
                        src="https://pngimage.net/wp-content/uploads/2018/05/empty-cart-png.png" 
                        alt="" 
                        style={{width:'100vw', height:'auto', marginTop:'100px'}} />
                    )
                }
                </div>
                <NavBottom/>
            </div>
        )
    }
}
