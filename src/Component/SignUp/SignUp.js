import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
// import {Link} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import EmailIcon from '@material-ui/icons/Email';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import PhoneIcon from '@material-ui/icons/Phone';
import LockIcon from '@material-ui/icons/Lock';
import checkNull from '../../shared/validation/ValidateForm'
import ValidateEmail from '../../shared/validation/ValidateEmail'
import checkPass from '../../shared/validation/ValidatePass'
import validateCfmPass from '../../shared/validation/ValidateCfmPass'
import validatePhone from '../../shared/validation/ValidatePhone'
import Axios from 'axios';

import './SignUp.css';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: 200,
  },
}));

export default function InputAdornments() {
    const [name,setName] = React.useState('');
    const [phone,setPhone] =React.useState('');
    const [email,setEmail] = React.useState('');
    const [pass,setPass] = React.useState('');
    const [cfmPass,setCfmPass] = React.useState('');

    const [invalidName,setInvalidName] = React.useState(false);
    const [invalidPhone,setInvalidPhone] = React.useState(false);
    const [invalidEmail,setInvalidEmail] = React.useState(false);
    const [invalidPass,setInvalidPass] = React.useState(false);
    const [invalidCfmPass,setInvalidCfmPass] = React.useState(false);

    const handleChangeEmail = (e) =>{
        setEmail(e.target.value)
    }
    const handleChangePhone = (e) =>{
        setPhone(e.target.value)
    }
    const handleChangeName = (e) =>{
        setName(e.target.value)
    }
    const handleChangePass = (e) =>{
        setPass(e.target.value)
    }
    const handleChangeCfmPass = (e) =>{
        setCfmPass(e.target.value)
    }

    const validatForm = () => {

        if(checkNull(name,phone,email,pass,cfmPass)){
            alert('Bạn chưa điền thông tin vào form ')
        }

        else if(checkNull(name) ){
            alert('Ban chua nhap Ten')
            setInvalidName(true)
        }
        // Phone Validate
        else if(checkNull(phone) ){
            alert('Ban chua nhap So Dien Thoai')
            setInvalidPhone(true)
        }
        else if(!validatePhone(phone)){
            alert('So Dien Thoai khong hop le')
            setInvalidPhone(true)
        }
        // Email Validate
        else if(checkNull(email) ){
            alert('Ban chua nhap Email')
            setInvalidEmail(true)
        }
        else if(!ValidateEmail(email))
        {
            alert('Email khong hop le')
            setInvalidEmail(true)
        }
        // Passwor Validate
        else if(checkNull(pass)){
            alert('Ban chua nhap Password')
            setInvalidPass(true)
        }
        else if(checkPass(pass)){
            setInvalidPass(true)
        }
        else if(checkNull(cfmPass)){
            alert('Ban chua nhap xac nhan Password')
            setInvalidCfmPass(true)
        }
        else if(!validateCfmPass(pass,cfmPass)){
            alert('Mat khau ko trung nhau')
            setInvalidPass(true)
            setInvalidCfmPass(true)
        }
        else if(!(invalidCfmPass && invalidEmail && invalidName && invalidPass && invalidPhone))
        {
            return true
        }
        else {
            return false;
        }
    }

    const Submit = (e) => {
        e.preventDefault();
        // Name Validate
        if(validatForm()) {
            sigUpSv(name, email, phone, pass);
        }
    }

    // AXIOS
    const sigUpSv = async (name, email, phone, pass) =>{
        var instancr = Axios.create({baseURL : 'https://waza-passenger.herokuapp.com/'})
        const res = await instancr.post('/api/passengers',{
            fullName:name,
            email: email,
            phone: phone,
            password: pass,
        })
        console.log(res)
        if(res) {
            window.location.href = "/account";
            alert('you are successfully sign up ')
        }
    }

    // MaterialUI 

    const classes = useStyles();
    const [values, setValues] = React.useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
    });
    const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
    };
    const handleMouseDownPassword = event => {
    event.preventDefault();
    };


  return (       
    <div id="signUpForm">
        <form onSubmit={Submit} className="form">

            <h1 style={{marginTop:'20px'}}>ĐĂNG KÝ</h1>
            <div className="field">
                <span className = "icn-log" > 
                    < AccountBoxIcon className = "icnName"
                        color = "disabled"
                        style = {
                            { 
                                marginTop:'25px',
                                marginRight:'10px',
                                fontSize: "default"
                            }
                        }> 
                    </AccountBoxIcon>
                </span >
                <TextField
                    className={classes.textField}
                    
                    onChange={handleChangeName}
                    error={invalidName}
                    id="name-Form"
                    value={name}
                
                    label="Tài Khoản"/>
            </div>

            <div className="field">
                <span className = "icn-log" > 
                    < PhoneIcon className = "icnPhone"
                        color = "disabled"
                        style = {
                            {
                                marginTop:'25px',
                                marginRight:'10px',
                                fontSize: "default"
                            }
                        }> 
                    </PhoneIcon>
                </span >
                <TextField
                    className={classes.textField}
                    
                    onChange={handleChangePhone}
                    error={invalidPhone}
                    id="phone-Form"
                    value={phone}
                
                    label="Số Điện Thoại" />
            </div>

            <div className="field">
            <span className = "icn-log" > 
                < EmailIcon className = "icnPass"
                color = "disabled"
                style = {
                    {
                        marginTop:'25px',
                        marginRight:'15px',
                        fontSize: "default"
                    }
                }> 
                </EmailIcon></span >
            <TextField
                error={invalidEmail}

                id="email-Form"
                value={email}

                className={classes.textField}
                onChange={handleChangeEmail}
                label="Email"/>
            </div>

            <div className="field" >
            <span className = "icn-log" > 
                <LockIcon 
                    className = "icnPass"
                    color = "disabled"
                    style = {
                    {
                        marginTop:'25px',
                        marginRight:'15px',
                        fontSize: "default"
                    }
                    } > 
                </LockIcon></span >
                <FormControl className={clsx(classes.margin, classes.textField)}>
                <InputLabel error={invalidPass} htmlFor="standard-adornment-password">Mật Khẩu</InputLabel>
                <Input
                    id="standard-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}

                    error={invalidPass}
                    value={pass}
                    onChange={handleChangePass}

                    endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        >
                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                    </InputAdornment>
                    }
                />
                </FormControl>
            </div>

            <div className="field">
            <span className = "icn-log" > 
                <LockIcon 
                    className = "icnPass"
                    color = "disabled"
                    style = {
                    {
                        marginTop:'25px',
                        marginRight:'15px',
                        fontSize: "default"
                    }
                    } > 
                </LockIcon></span >
                <FormControl className={clsx(classes.margin, classes.textField)}>
                <InputLabel error={invalidCfmPass} htmlFor="standard-adornment-password">Xác Nhận Mật Khẩu</InputLabel>
                <Input
                    id="standard-adornment-cfmpassword"
                    type={values.showPassword ? 'text' : 'password'}

                    error={invalidCfmPass}
                    value={cfmPass}
                    onChange={handleChangeCfmPass}

                    endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        >
                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                    </InputAdornment>
                    }
                />
                </FormControl>
            </div>

            <button className ="btn-lg"
                onClick={sigUpSv}
                type = "submit" >
                Đăng Ký 
            </button> 
        </form>
    </div>     
  );
}
