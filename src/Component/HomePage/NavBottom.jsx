import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab'; 
import {Link} from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import PersonIcon from '@material-ui/icons/Person';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    width:'100vw',
    position:'fixed',
    top:'auto',
    height:'50px',
    bottom:'0',
  },
});

export default function IconLabelTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    console.log(newValue)
    setValue(newValue);
  };

  return (
    <Paper square className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        variant="fullWidth"
        indicatorColor="secondary"
        textColor="secondary"
      >
        <Tab component={Link} aria-label="phone" to="/" icon={<HomeIcon />}/>

        <Tab component={Link} to="/cart" icon={<ShoppingBasketIcon />}/>
        {
          localStorage.getItem('token') ? (
            <Tab component={Link} to="/detailsAccount" icon={<PersonIcon />} />
          ):
          (
            <Tab component={Link} to="/account" icon={<PersonIcon />} />
          )
        }
      </Tabs>
    </Paper>
  );
}

