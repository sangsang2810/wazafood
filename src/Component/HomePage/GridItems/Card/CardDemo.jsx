import React, { Component } from 'react'
import axios from 'axios';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";

export default class CardDemo extends Component {

  
  constructor(props) {
    super(props)

  this.state = {
      items: []
      
    }
  }
    async componentDidMount(){
      axios({
        method:'GET',
        url:'http://103.82.240.199:9080/final-excercise/api/restaurant/list',
        data:{"indexRequest":0, "returnRecords":50}
      }).then( res => {
        // const items = res.data.listResult;
        localStorage.setItem('res',JSON.stringify(res));
        this.setState({items:res.data.listResult })
      }).catch(err => {
        console.log(err)
      })
  }

  handleOnClick = (item) =>{
    localStorage.setItem('restID', item.restaurantId);
  }

  render() {
    // const datafilter = this.state.items.filter(x => x.restaurant_category === this.props.filter);
    return (
      <div style={{display:'flex',flexWrap:'wrap'}}>
        {this.state.items.map((item,index) => {
            return <Card key={index} style={{marginBottom:'5px',marginRight:'5px',width:'48%'}}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                height="140"
                src={item.restaurantImage}
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography style={{textAlign:'left',overflow:'hidden',textOverflow:'ellipsis',whiteSpace:'nowrap'}} gutterBottom variant="h6" component="h6">
                  {item.restaurant_name}
                </Typography>
                <Typography style={{textAlign:'left',overflow:'hidden',textOverflow:'ellipsis',whiteSpace:'nowrap'}} variant="body2" color="textSecondary" component="p">
                  {'Đ/c: ' + item.restaurant_address}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Link to="/fooddetail">
                <Button 
                  onClick={() => localStorage.setItem('restID', item.restaurantId)} 
                  size="small" color="primary">
                  Chi tiết
                </Button>
              </Link>
            </CardActions>
          </Card>
        })}
      </div>
    )
  }
}


// const datafilter = this.state.items.filter(x => x.category === this.props.filter);
// return (
  // <div>
  //   {datafilter.map(item => {
  //     return item.lstFood.map((food,foodid) => {
  //       return <Card key={foodid}>
  //     <CardActionArea>
  //       <CardMedia
  //         component="img"
  //         alt="Contemplative Reptile"
  //         height="140"
  //         src="https://nipimedia.com/wp-content/uploads/2019/08/Banh-mi-Nipimedia-8.jpg"
  //         title="Contemplative Reptile"
  //       />
  //       <CardContent>
  //         <Typography gutterBottom variant="h5" component="h2">
  //           {item.restaurant_name}
  //         </Typography>
  //         <Typography variant="body2" color="textSecondary" component="p">
  //           {food.name_food}
  //         </Typography>
  //       </CardContent>
  //     </CardActionArea>
  //     <CardActions>
  //       <Button size="small" color="primary">
  //         Share
  //       </Button>
  //       <Button size="small" color="primary">
  //         Learn More
  //       </Button>
  //     </CardActions>
  //   </Card>
  //     })   
  //   })}
  // </div>
// )