import React, { Component } from 'react';
import './Card.css';
import {Link} from "react-router-dom";
import dataItems from '../../../../contants/food';


export default class Card extends Component { 
    constructor() {
      super();
      this.state = {
        data:[]
      }
    }
    componentWillMount() {
      this.setState({
        posts: dataItems,
        }); 
     
    }
   
    
    render() {
      const datafilter = dataItems.filter(x => x.category === this.props.filter);

      

      return <div className="cardView">
        
         
        <div className="app-card-list" id="app-card-list">
          {
            // Object
            // .keys(this.state.posts)
            // .map(key => <CardView key={key} index={key} details={this.state.posts[key]}/>)
            
            datafilter.map ((x,i) => {
              return (
                <CardView key={i} category={x.category} image={x.image} text={x.text} title={x.title}/>
              )
            })
          }
      </div>
    </div>
  }
}
  
  class Button extends React.Component {
    render() {
      return (
        <Link to="/fooddetail">
          <button className="button-primary">
            XEM NGAY
          </button>
        </Link>
      )
    }
  }
  
  class CardHeader extends React.Component {
    render() {
      const { image, category } = this.props;
      var style = { 
          backgroundImage: 'url(' + image + ')',
          height: '50%',
          width: '100%',  
      };
      return (
        <header style={style} className="card-header">
          <h4 className="card-header--title">{category}</h4>
        </header>
      )
    }
  }
 
  class CardBody extends React.Component {
    render() {
      return (
        <div className="card-body">
          <h4 style={{margin:"0",width:"100%"}}>{this.props.title}</h4>
          <p className="body-content">{this.props.text}</p>
          <Button/>
        </div>
      )
    }
  }
  
  class CardView extends React.Component {
    render() {
      return (
        <article className="card">
          <CardHeader  category={this.props.category} image={this.props.image} className="cardHeader" />
          <CardBody title={this.props.title} text={this.props.text} className="cardBody"/>
        </article>
      )
    }
  }