import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
// import Card from './Card/Card';
import CardDemo from './Card/CardDemo'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}>
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom:'50px',
    backgroundColor: "#E0E1E1",
    height:'auto',
    border: '1px solid white' ,
  },

  appbar: {
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    color:"tomato",
    padding:"2px",
    backgroundColor: "white",
  },


}));



export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appbar}>
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab style={{outline:'none',float:'left'}} label="Gợi ý cho bạn !" {...a11yProps(0)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0} >
        <CardDemo filter='Food' />
      </TabPanel>
      <TabPanel  value={value} index={1}  >
      <CardDemo filter='Drink' />
      </TabPanel>
    </div>
  );
}