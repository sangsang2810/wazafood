import React from 'react'
import "./Menu.css";
// import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import {Link} from "react-router-dom";
import IconButton from '@material-ui/core/IconButton';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import PersonRoundedIcon from '@material-ui/icons/PersonRounded';

const Menu = () => {
    const [isShow, setIsShow] = React.useState(false);
    
    const handleOnClick = () =>{
        isShow ? setIsShow(false) : setIsShow(true)
        console.log(isShow)
    }
        const token = localStorage.getItem('token')
        return (
        <div className="header">
           <Link style={{textDecoration:"none"}} to="/" className="logo">WAZAFOOD</Link>

            <span className="navbar-toggle">
            <IconButton style={{outline:'none' }} onClick={handleOnClick}>
                {isShow ?(
                    <MenuOpenIcon style={{ fontSize:'32px', color:"white", float:"right",cursor: 'pointer', outline:'none' }}/>
                    )
                    :(
                        <MenuIcon style={{ fontSize:'32px', color:"white", float:"right",cursor: 'pointer',outline:'none' }}/>
                    )
                }
            </IconButton>
            </span>

            {isShow ? (
                <div className="menuMobile" id="menux">
                <Link className="items" style={{textDecoration:"none"}} to="/foodpage">
                    <b className="btn-pageItems">ĐỒ ĂN</b>
                    </Link>
                
                <Link className="items" style={{textDecoration:"none"}} to="/drinkpage">
                    <b className="btn-pageItems">NƯỚC UỐNG</b>
                    </Link>
         
                <Link className="items" style={{textDecoration:"none"}} to="/booking">
                    <b className="btn-pageItems">ĐẶT BÀN</b>
                    </Link>
                
                <Link className="items" style={{textDecoration:"none"}} to="/service">
                    <b className="btn-pageItems">DỊCH VỤ</b>
                    </Link>
 
            </div>
            ): <div></div>}

 
            
            <ul className="menu">
                <li className="btn-pageItems">
                    <Link style={{textDecoration:"none"}} className="btn-pageItems" to="/foodpage">
                    ĐỒ ĂN
                    </Link>
                </li>

                <li className="btn-pageItems">
                    <Link style={{textDecoration:"none"}} className="btn-pageItems" to="/drinkpage">
                    NƯỚC UỐNG
                    </Link>
                </li>

                <li className="btn-pageItems">
                    <Link style={{textDecoration:"none"}} className="btn-pageItems" to="/booking">
                    ĐẶT BÀN
                    </Link>
                </li>

                <li className="btn-pageItems">
                    <Link style={{textDecoration:"none"}} className="btn-pageItems" to="/service">
                    DỊCH VỤ
                    </Link>
                </li>
                
                <li className="search">
                    <input type="text" className="searchTerm" placeholder="Bạn muốn tìm ?"/>
                    <button type="submit" className="searchButton"><SearchIcon ></SearchIcon></button>
                </li>

                <li className="icn-acc">
                    {token ? (
                        <PersonRoundedIcon  style={{ fontSize:'30px', color:"white" }} >
                        {/*  */}
                        </PersonRoundedIcon>
                    ) : 
                    (
                        <Link to="/account"
                        fontSize="large" 
                        onClick={() => {}} 
                        style={{marginTop:"-5px", marginLeft:"20px", textDecoration:"none"}} >
                        <Button style={{color:"white"}}>ĐĂNG NHẬP</Button>
                        </Link>
                    )}
                </li>  
                
                <li className="icn-cart" style={{width:"60px"}}>
                    <Link to="/cart">
                    <Button>
                        <ShoppingCartIcon   style={{ fontSize:'25px', color:"white", float:"right" }}/> 
                    </Button> 
                    </Link>    
                </li>
            </ul>
        </div>
        )
    }

export default Menu ;