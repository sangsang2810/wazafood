import React, { Component } from 'react'

import{Carousel,} from 'react-bootstrap';
import './Slideshow.css';

export default class SliderDemo extends Component {
    render() {
        return (
            <div className="sliderDemo" >
               <Carousel>
                <Carousel.Item>
                    <img
                    className="rp-size d-block w-100"
                    src={require('./img/1.jpg')}
                    alt="First slide"
                    />
                </Carousel.Item>

                <Carousel.Item>
                    <img
                    className=" rp-size d-block w-100"
                    src={require('./img/4.jpg')}
                    alt="Third slide"
                    />
                </Carousel.Item>

                <Carousel.Item>
                    <img
                    className="rp-size d-block w-100 h-10"
                    src={require('./img/3.jpg')}
                    alt="Third slide"
                    />
                </Carousel.Item>
                </Carousel>
            </div>
        )
    }
}
