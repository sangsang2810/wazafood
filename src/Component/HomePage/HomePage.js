import React, { Component } from 'react'
import Menu from './MenuBar/Menu';
import GridItems from './GridItems/GridItems';
import SlideDemo from './Slider/SliderDemo'
import NavBottom from './NavBottom'


export default class HomePage extends Component {
    render() {
        return (
            <div style={{backgroundColor: "#E0E1E1",height:'100vh'}}>
                <Menu/>
                <SlideDemo></SlideDemo>
                <GridItems></GridItems>
                <NavBottom history={this.props.history} ></NavBottom>
            </div>
        )
    }
}
