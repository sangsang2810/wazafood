import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import checkNull from '../../shared/validation/ValidateForm'
import ValidateEmail from '../../shared/validation/ValidateEmail'
import checkPass from '../../shared/validation/ValidatePass'
import Axios from 'axios';
import './Acc.css';
import NavBottom from '../HomePage/NavBottom';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: 200,
  },
}));

export default function InputAdornments() {
    const [email,setEmail] = React.useState('');
    const [pass,setPass] = React.useState('');
    const [invalidEmail,setInvalidEmail] = React.useState(false);
    const [invalidPass,setInvalidPass] = React.useState(false);

    const handleChangeEmail = (e) =>{
        setEmail(e.target.value)
    }
    const handleChangePass = (e) =>{
        setPass(e.target.value)
    }
    const validateSigIn=()=>{
        
        if(checkNull(email && pass)  ){
            alert('Email và Mật Khẩu không được để trống')
            setInvalidEmail(true)
            setInvalidPass(true)
        }
        
        else if(!ValidateEmail(email))
        {
            alert('Email không hợp lệ')
            setInvalidEmail(true)
        }
        else if(checkNull(pass)){
            alert("Bạn chưa nhập Mật Khẩu")
            setInvalidPass(true)
        }
        else if(checkPass(pass)){
            setInvalidPass(true)
        }
        if(!(invalidEmail && invalidPass))
        {
            return true;
        }
        else{
            return false;
        }
    }

    const sigInSv = async ( email, pass) =>{
        var instancr = Axios.create({baseURL : 'https://waza-passenger.herokuapp.com/'})
        const res = await instancr.post('/api/passengers/auth',{
            email: email,
            password: pass,
        })  
        if(res) {
            alert('Đăng nhập thành công !')
            window.location.href = "/";
            localStorage.setItem('token',res.data.token)
        }
        else{
            alert('Tài khoản hoặc mật khẩu không chính xác')
        }
    }

    const SubmitSignIn = (e) => {
        e.preventDefault();
        if(validateSigIn()){
            sigInSv(email, pass);
            // window.location.href= "/"
        }
    }
    // MaterialUI 
    const classes = useStyles();
    const [values, setValues] = React.useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
    });
    const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
    };
    const handleMouseDownPassword = event => {
    event.preventDefault();
    };
  return (       
        <div id="loginForm">
           <form onSubmit={SubmitSignIn} className="form">
            <h1 style={{marginTop:'20px'}}>ĐĂNG NHẬP</h1>
           <div className="emailForm">
            <span className = "icn-log" > 
                < EmailIcon className = "icnPass"
                color = "disabled"
                style = {
                    {
                        marginTop:'25px',
                        marginRight:'15px',
                        fontSize: "default"
                    }
                }> 
                </EmailIcon></span >
            <TextField
                error={invalidEmail}
                id="standard-basic"
                value={email}
                className={classes.textField}
                onChange={handleChangeEmail}
                label="Email"
                margin="normal"/>
            </div>

            <div className="passForm" >
            <span className = "icn-log" > 
                <LockIcon color = "disabled"
                    className = "icnPass"
                    style = {
                        {
                            marginTop:'20px',
                            marginRight:'5px',
                            fontSize: "default"
                        }
                    } > 
                </LockIcon>
            </span >
                <FormControl className={clsx(classes.margin, classes.textField)}>
                <InputLabel htmlFor="standard-adornment-password">Mật Khẩu</InputLabel>
                <Input
                    id="standard-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}

                    error={invalidPass}
                    value={pass}
                    onChange={handleChangePass}

                    endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        >
                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                    </InputAdornment>
                    }
                />
                </FormControl>
            </div>

            <button className ="btn-lg"
                onClick={sigInSv}
                type = "submit" >
                Đăng Nhập 
            </button> 
            <h6 style={{marginTop:'20px'}}> Bạn chưa có tài khoản ? < Link to = "/signup" > Đăng ký ngay </Link></h6>
           </form>
           <NavBottom/>
        </div>
  );
}
