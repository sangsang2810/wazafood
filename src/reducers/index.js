import { combineReducers} from 'redux';
import product from '../contants/food';

const appReducers =  combineReducers({ product });

 export default appReducers;