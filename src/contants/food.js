const dataItems = [{
    id: 0,
    restaurant: "X",
    category: "Food",
    title: "Hamburger",
    price: 25000,
    text: "CNN purchased Casey Neistat's Beme app for $25million.",
    image: "http://www.monngon.tv/uploads/images/2017/06/26/7210c4294d10ad2375fccf9e305f86fa-lam-banh-hamburger-sl.jpg"
  },
  {
    id: 1,
    restaurant: "X",
    category: "Food",
    title: "Spaghetti",
    price: 15000,
    text: "Learn our tips and tricks on living a nomadic lifestyle",
    image: "http://www.monngon.tv/uploads/images/2017/06/26/7210c4294d10ad2375fccf9e305f86fa-lam-banh-hamburger-sl.jpg"
  },
  {
    id: 2,
    restaurant: "X",
    category: "Food",
    title: "Bakon",
    price: 35000,
    text: "The first ever decoupled starter theme for React & the WP-API",
    image: "https://thumbs.dreamstime.com/z/bakon-4207495.jpg"
  },
  {
    id: 3,
    restaurant: "X",
    category: "Drink",
    title: "CNN Acquire BEME",
    price: 23000,
    text: "CNN purchased Casey Neistat's Beme app for $25million.",
    image: "https://source.unsplash.com/user/erondu/600x400"
  },
  {
    id: 4,
    restaurant: "X",
    category: "Drink",
    title: "Nomad Lifestyle",
    price: 5000,
    text: "Learn our tips and tricks on living a nomadic lifestyle",
    image: "https://source.unsplash.com/user/_vickyreyes/600x400"
  },
  {
    id: 5,
    restaurant: "X",
    category: "Drink",
    title: "React and the WP-API",
    price: 35000,
    text: "The first ever decoupled starter theme for React & the WP-API",
    image: "https://source.unsplash.com/user/ilyapavlov/600x400"
  }
]

export default dataItems;